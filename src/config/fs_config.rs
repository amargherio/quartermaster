use serde::Deserialize;

#[derive(Deserialize)]
pub struct FileSystemSourceConfiguration {
    pub source_dir: String,
}

impl FileSystemSourceConfiguration {
    fn new(source_dir: &str) -> FileSystemSourceConfiguration {
        FileSystemSourceConfiguration {
            source_dir: String::from(source_dir),
        }
    }
}
