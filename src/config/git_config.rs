use serde::Deserialize;

#[derive(Deserialize)]
pub struct GitRepositorySourceConfiguration {
    repository_url: String,
    source_dir: String,
    should_reload: bool,
}
