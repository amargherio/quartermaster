pub mod fs_config;
pub mod git_config;

use chrono::prelude::*;
use serde::Deserialize;
use serde_json;
use serde_yaml;
use toml;

use fs_config::FileSystemSourceConfiguration;
use git_config::GitRepositorySourceConfiguration;

use std::env;
use std::fs::File;
use std::fs;
use std::io::{BufReader, Error, ErrorKind};
use std::io::prelude::*;
use std::path::{Path, PathBuf};

#[derive(Deserialize)]
pub struct QMConfig {
    load_timestamp: DateTime<Utc>,
    fs_config: FileSystemSourceConfiguration,
    git_config: GitRepositorySourceConfiguration,
    default_log_level: String,
    http_port: Option<u16>,
    push_config_updates: bool,
}

/// Loads the Quartermaster configuration file from a variety of sources.
///
/// The supported configuration formats as of now are TOML, YAML, and JSON, in order of load preference.
/// Only one config is supported - it's required that all configuration be placed into a single file as multiple
/// configurations won't be honored or loaded.
///
/// The first location checked during load is the `QM_CONFIG` environment variable - if found, the default
/// configuration paths will be ignored in favor of the file located at the path in-variable.
///
/// If `QM_CONFIG` is empty, the default path of `/opt/quartermaster/config` is used. The directory will be searched
/// for a `qm_config` file with any of the supported extensions. When one is found, additional searching and loading
/// will be halted.
///
/// If no configuration file is found in `/opt/quartermaster/config`, an error will be returned and logged and the
/// application terminated.
pub fn parse_config() -> Result<QMConfig, Error> {
    let mut cfg_loc: PathBuf;
    let mut cfg_ext = String::new();
    cfg_loc = match env::var_os("QM_CONFIG") {
        Some(val) => PathBuf::from(&val),
        None => {
            let path_root = Path::new("/opt/quartermaster/config");
            let extensions = vec!["toml", "yaml", "json"];

            for extz in extensions {
                let p = path_root.with_extension(extz);
                if p.exists() && p.is_file() {
                    cfg_ext = String::from(extz);
                    break;
                }
            }
            path_root.with_extension(cfg_ext)
        },
    };

    // depending on the file extension, load the correct serde parser (toml, yaml, or json),
    // read the file, and start parsing it
    let config_ext = cfg_loc.extension().unwrap();
    let mut config: QMConfig;

    let cfg = File::open(&cfg_loc)?;
    let mut buf_reader = BufReader::new(cfg);
    let mut cfg_str = String::new();
    buf_reader.read_to_string(&mut cfg_str);

    if config_ext == "toml" {
        config = toml::from_str(cfg_str.as_str()).unwrap();
        Ok(config)
    } else if config_ext == "yaml" {
        config = serde_yaml::from_str(cfg_str.as_str()).unwrap();
        Ok(config)
    } else if config_ext == "json" {
        // treat it as JSON, since that's the only other option.
        config = serde_json::from_str(cfg_str.as_str())?;
        Ok(config)
    } else {
        Err(Error::new(ErrorKind::NotFound, "No acceptable config file has been found."))
    }
}


#[cfg(test)]
mod tests {
    #[test]
    fn envvar_config_path() {
        assert_eq!("1", "1")
    }

    #[test]
    fn no_envvar_config_path() {
        assert_eq!("1", "1")
    }

    #[test]
    fn toml_config_parse() {
        // in general, idea here is to set the config path, let it get
        // parsed in, and then compare the QMConfig returned from the parse
        // function with one parsed here to determine if they're equal
        assert_eq!("1", "1")
    }

    #[test]
    fn yaml_config_parse() {
        // same as docs in toml_config_parse test
        assert_eq!("1", "1")
    }

    #[test]
    fn json_config_parse() {
        // same as docs in toml_config_parse test
        assert_eq!("1", "1")
    }

    #[test]
    fn bare_server_config() {}

    #[test]
    fn no_server_config() {}

    #[test]
    fn single_source_block() {
    }

    #[test]
    fn multiple_source_blocks() {}

}
