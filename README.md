# Quartermaster

<hr>

Quartermaster is a system for managing and distributing runtime system and application configurations. It takes in a variety of formats and configuration sources
and serves consolidated configuration objects in multiple supported formats.


## Features and Roadmap
For the **v1.0** release of Quartermaster:

* Support for HTTP, HTTP/2, and gRPC requests
* Configuration retrieval from Git repositories
* Dynamically reloading stored configuration files via webhooks and triggers
* Support for TOML, JSON, YAML, and propertyfile configuration files

While we can't put a timeline yet on these features and functions, our eventual feature suite will include:

* Request proxying to additional backend configuration services
* Push-based configuration updates (pushing updated configurations to downstream consumers, when opted in)
* Configuration diffs and checkpoints - see what changed in the generated configurations at a specific point in time
* Integration with backing databases and distributed stores for alternative configuration serving
  * [ ] Redis
  * [ ] Cassandra
  * [ ] Consul
